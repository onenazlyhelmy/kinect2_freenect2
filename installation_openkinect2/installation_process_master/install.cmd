apt install  build-essential cmake pkg-config libusb-1.0-0-dev libturbojpeg0-dev libglfw3-dev beignet-dev libopenni2-dev -y
┌─[root@parrot]─[/storage1/DEVELOPMENT/PYTHON/freenect/OpenKinect2_installation/installation_process_master/libfreenect2/build]
└──╼ #ls
bin             CMakeFiles           doc       freenect2Config.cmake         freenect2.pc  libfreenect2  resources.inc.h
CMakeCache.txt  cmake_install.cmake  examples  freenect2ConfigVersion.cmake  lib           Makefile
┌─[root@parrot]─[/storage1/DEVELOPMENT/PYTHON/freenect/OpenKinect2_installation/installation_process_master/libfreenect2/build]
└──╼ #cmake .. -DCMAKE_INSTALL_PREFIX=/usr/lib/freenect2
-- using tinythread as threading library
-- Checking for modules 'libva;libva-drm'
--   No package 'libva' found
--   No package 'libva-drm' found
-- Could NOT find JPEG (missing: JPEG_LIBRARY JPEG_INCLUDE_DIR) 
-- Could NOT find TegraJPEG (missing: TegraJPEG_LIBRARIES TegraJPEG_INCLUDE_DIRS TegraJPEG_WORKS) 
CMake Warning (dev) at /usr/share/cmake-3.13/Modules/FindOpenGL.cmake:270 (message):
  Policy CMP0072 is not set: FindOpenGL prefers GLVND by default when
  available.  Run "cmake --help-policy CMP0072" for policy details.  Use the
  cmake_policy command to set the policy and suppress this warning.

  FindOpenGL found both a legacy GL library:

    OPENGL_gl_LIBRARY: /usr/lib/x86_64-linux-gnu/libGL.so

  and GLVND libraries for OpenGL and GLX:

    OPENGL_opengl_LIBRARY: /usr/lib/x86_64-linux-gnu/libOpenGL.so
    OPENGL_glx_LIBRARY: /usr/lib/x86_64-linux-gnu/libGLX.so

  OpenGL_GL_PREFERENCE has not been set to "GLVND" or "LEGACY", so for
  compatibility with CMake 3.10 and below the legacy GL library will be used.
Call Stack (most recent call first):
  CMakeLists.txt:269 (FIND_PACKAGE)
This warning is for project developers.  Use -Wno-dev to suppress it.

CUDA_TOOLKIT_ROOT_DIR not found or specified
-- Could NOT find CUDA (missing: CUDA_TOOLKIT_ROOT_DIR CUDA_NVCC_EXECUTABLE CUDA_INCLUDE_DIRS CUDA_CUDART_LIBRARY) 
-- Linking with these libraries: 
 /usr/lib/x86_64-linux-gnu/libusb-1.0.so
 pthread
 /usr/lib/x86_64-linux-gnu/libturbojpeg.so.0
 /usr/lib/x86_64-linux-gnu/libglfw.so
 /usr/lib/x86_64-linux-gnu/libGL.so
 /usr/lib/x86_64-linux-gnu/libOpenCL.so
-- Could NOT find Doxygen (missing: DOXYGEN_EXECUTABLE) 
-- Configurating examples
-- Using in-tree freenect2 target
-- Feature list:
--   CUDA    no
--   CXX11    disabled
--   Examples    yes
--   OpenCL    yes
--   OpenGL    yes
--   OpenNI2    yes
--   TegraJPEG    no
--   Threading    tinythread
--   TurboJPEG    yes
--   VAAPI    no
--   VideoToolbox    no (Apple only)
--   streamer_recorder    disabled
-- Configuring done
-- Generating done
-- Build files have been written to: /storage1/DEVELOPMENT/PYTHON/freenect/OpenKinect2_installation/installation_process_master/libfreenect2/build
┌─[root@parrot]─[/storage1/DEVELOPMENT/PYTHON/freenect/OpenKinect2_installation/installation_process_master/libfreenect2/build]
└──╼ #make
[  5%] Built target generate_resources_tool
[ 66%] Built target freenect2
[ 89%] Built target freenect2-openni2
[100%] Built target Protonect
┌─[root@parrot]─[/storage1/DEVELOPMENT/PYTHON/freenect/OpenKinect2_installation/installation_process_master/libfreenect2/build]
└──╼ #make install
[  5%] Built target generate_resources_tool
[ 66%] Built target freenect2
[ 89%] Built target freenect2-openni2
[100%] Built target Protonect
Install the project...
-- Install configuration: "RelWithDebInfo"
-- Installing: /usr/lib/freenect2/lib/libfreenect2.so.0.2.0
-- Installing: /usr/lib/freenect2/lib/libfreenect2.so.0.2
-- Installing: /usr/lib/freenect2/lib/libfreenect2.so
-- Installing: /usr/lib/freenect2/include/libfreenect2
-- Installing: /usr/lib/freenect2/include/libfreenect2/frame_listener.hpp
-- Installing: /usr/lib/freenect2/include/libfreenect2/registration.h
-- Installing: /usr/lib/freenect2/include/libfreenect2/led_settings.h
-- Installing: /usr/lib/freenect2/include/libfreenect2/logger.h
-- Installing: /usr/lib/freenect2/include/libfreenect2/color_settings.h
-- Installing: /usr/lib/freenect2/include/libfreenect2/packet_pipeline.h
-- Installing: /usr/lib/freenect2/include/libfreenect2/frame_listener_impl.h
-- Installing: /usr/lib/freenect2/include/libfreenect2/libfreenect2.hpp
-- Up-to-date: /usr/lib/freenect2/include/libfreenect2
-- Installing: /usr/lib/freenect2/include/libfreenect2/export.h
-- Installing: /usr/lib/freenect2/include/libfreenect2/config.h
-- Installing: /usr/lib/freenect2/lib/cmake/freenect2/freenect2Config.cmake
-- Installing: /usr/lib/freenect2/lib/cmake/freenect2/freenect2ConfigVersion.cmake
-- Installing: /usr/lib/freenect2/lib/pkgconfig/freenect2.pc
-- Installing: /usr/lib/freenect2/lib/OpenNI2/Drivers/libfreenect2-openni2.so.0
-- Set runtime path of "/usr/lib/freenect2/lib/OpenNI2/Drivers/libfreenect2-openni2.so.0" to ""
-- Installing: /usr/lib/freenect2/lib/OpenNI2/Drivers/libfreenect2-openni2.so
┌─[root@parrot]─[/storage1/DEVELOPMENT/PYTHON/freenect/OpenKinect2_installation/installation_process_master/libfreenect2/build]
└──╼ #

